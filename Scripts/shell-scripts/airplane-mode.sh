#!/bin/sh

# This will turn off wifi, data and bluetooth
# and turn on airplane mode

# Turn off data
DATA=$(nmcli radio wwan)

if [ "$DATA" = "enabled" ]; then
    $(nmcli radio wwan off)
    $(notify-send "Mobile data turned off")
fi

# turn off wifi
WIFI=$(nmcli radio wifi)
if [ "$WIFI" = "enabled" ]; then
    $(nmcli radio wifi off)
    $(notify-send "Wifi turned off")
fi

# turn off bluetooth

