extends Area2D

var previous_mouse_position = Vector2()
var is_dragging = false

func _on_TopBar_input_event(viewport, event, shape_idx):
	"""
	Allows dragging the surface is the 
	user clicks inside the collision shape
	"""
	#print("Touched!")
	if event.is_action_pressed("ui_touch"):
		print(event)
		get_tree().set_input_as_handled()
		previous_mouse_position = event.position
		is_dragging = true

func _input(event):
	"""
	Works globally so dragging works if
	the mouse cursor is outside the collion
	shape
	Disables dragging if the user releases click
	"""
	
	if not is_dragging:
		return
	
	if event.is_action_released("ui_touch"):
		previous_mouse_position = Vector2()
		is_dragging = false
	
	if is_dragging and event is InputEventMouseMotion:
		position += event.position - previous_mouse_position
		previous_mouse_position = event.position
		


func _on_TopBarButton_pressed():
	print("called")
	$Transition.interpolate_property(self, "position", Vector2(0,0), Vector2(0,100), 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Transition.start()
	#pass # Replace with function body.
