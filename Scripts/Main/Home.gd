extends Control

var pressed = false
var time_pressed = 0.0

var favs = preload("res://Scenes/AppWheel.tscn")

func _ready():
	set_process_input(true)

func _input(event):
	if Input.is_action_pressed("ui_touch"):
		print("UI Touched")
		pressed = true
	if Input.is_action_just_released("ui_touch"):
		pressed = false

func _physics_process(delta):
	if pressed == true:
		time_pressed += delta
	else:
		time_pressed = 0

	if time_pressed > 1.25:
		var load_favs = favs.instance()
		load_favs.position = Vector2(0,0)
		add_child(load_favs)
