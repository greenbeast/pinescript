extends RichTextLabel

func _ready():
	bbcode_enabled = true
	# low processor usage since PP is WEAK
	OS.low_processor_usage_mode = true
	
	var output = []
	# var path = "/home/hank/Games/Godot/GDPhone/Scripts/shell-scripts/wifi.sh"
	var path = "res://Scripts/shell-scripts/wifi.sh"
	
	OS.execute(path,[], true, output)
	if output[0].strip_edges() == "enabled":
		print("Wifi on")
		globals.wifi_on = true

	else:
		print("Wifi Off, but script is running")
		globals.wifi_on = false


func _process(delta):
	var date_time = OS.get_datetime(false)
	var display_time = ""
	#print(date_time)
	# Sets out date time to be bold
	if date_time["minute"] < 10:
		var mins = "0"+str(date_time["minute"])
		display_time = str(date_time["hour"])+":"+mins
	else:
		display_time = str(date_time["hour"])+":"+str(date_time["minute"])
		
	if globals.wifi_on == true:
		display_time = display_time.strip_edges()
		display_time += "[center]wifi[/center]"
	else:
		display_time = display_time.strip_edges()
		
		display_time += "     [img]"+"res://Assets/wifi-16.png"+"[/img]"

	set_bbcode("[right][b]"+display_time+"[/b][/right]")


func _on_TopBarButton_pressed():
	var display_time = " "
	set_visible_characters(0)
	#set_bbcode(display_time)
	print("Called press")
